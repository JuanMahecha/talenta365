# encoding: utf-8
#VERSION EJECUTABLE EN PYTHON3
#EJECUTE DESDE SU CONSOLA ==> python3 alphabet.py
import math
import sys

if sys.version_info[0] < 3:
	try:
		from imp import reload
		reload(sys)  
		sys.setdefaultencoding('utf8')
	except:
		pass
		
class Alphabet:
	
	def __init__(self):
		
		self.abc_list = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','Ñ','O','P','Q','R','S','T','U','V','W','X','Y','Z']	
		self.abc_len = len(self.abc_list)
			
	def get_column_number(self):
		correct_number = False
		attempts = 1
		while not correct_number:			
			column_number = input("Digite el numero de la columna que desea obtener: ")
			try:
				column_number = int(column_number)
				correct_number = True
			except:
				if attempts >= 5:
					print("INTENTOS MAXIMOS SUPERADOS, INTENTELO MAS TARDE")
					exit()
				else:
					attempts+=1
		response = self.get_alphabet(column_number)
		return response
		
	def get_alphabet(self,size):
		
		end = False
		response = ""
		cycles = 0
		residue = 0
		while not end:			
			decimal, cycles = math.modf(size / self.abc_len)					
			residue = size - (cycles * self.abc_len)			
			response = self.abc_list[int(residue-1)] + response
			
			if residue == 0:
				cycles -= 1							
			
			if cycles == 0:
				end = True
			else:
				size = cycles
			
		return response

abc =  Alphabet()

abc_text = abc.get_column_number()

if sys.version_info[0] < 3:
	print(u"COLUMNA {}".format(abc_text))
else:
	print("COLUMNA ", abc_text)	
	
