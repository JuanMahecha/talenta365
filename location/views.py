import json
from django.shortcuts import render
from .helpers.city_helper import CityHelper
from .helpers.region_helper import RegionHelper
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template.loader import get_template, render_to_string
from weasyprint import HTML, CSS

# Create your views here.

def index(request):

    context = {
        "title": "TALENTA365",
    }
    return render(request, 'location/index.html', context)


def city(request):  

    city_helper = CityHelper()  
    context = city_helper.make_context()       
    return render(request, 'location/city.html', context)

def city_save(request):

    if(request.method == "POST"):		
        request_body = json.loads(request.body)           
        object_id = request_body.get("id")
        object_code = request_body.get("code")
        object_text = request_body.get("text")        
        object_status = request_body.get("status")

        city_helper = CityHelper()  
        response_status, response_data = city_helper.save(object_id, object_code, object_text, object_status)                   
       
        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json",
            status=response_status
        )
    else:
        return redirect('/city/')

def city_edit(request):

    if(request.method == "POST"):		
        request_body = json.loads(request.body)
        object_id = request_body.get("id")
        
        city_helper = CityHelper()  
        response_status, response_data = city_helper.find_by_id(object_id)                   
        
        return HttpResponse(
            json.dumps(response_data),
            content_type = "application/json",
            status = response_status
        )
    else:
        return redirect('/city/')

def city_delete(request):

    if(request.method == "DELETE"):		
        request_body = json.loads(request.body)
        object_id = request_body["id"]
        
        city_helper = CityHelper()  
        response_status, response_data = city_helper.delete_by_id(object_id)                   

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json",
            status = response_status
        )
    else:
        return redirect('/city/')

def city_report(request):

    path= 'location/reports/city.html'
    city_helper = CityHelper()  
    context = city_helper.make_report_context()
    html_template = render_to_string(path, context)
    pdf_file = HTML(string=html_template).write_pdf()
    response = HttpResponse(pdf_file, content_type='application/pdf')
    response['Content-Disposition'] = 'filename="reporte_municipios.pdf"'
    return response

def region(request):   

    region_helper = RegionHelper()  
    context = region_helper.make_context()       
    return render(request, 'location/region.html', context)

def region_save(request):
    
    if(request.method == "POST"):		
        request_body = json.loads(request.body)               
        object_code = request_body.get("code")
        object_text = request_body.get("text")
        object_id = request_body.get("id")        
        object_cities_id = request_body.get("cities_id")        
        
        region_helper = RegionHelper()  
        response_status, response_data = region_helper.save(object_id, object_code, object_text, object_cities_id) 

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json",
            status=response_status
        )
    else:
        return redirect('/region/')

def region_edit(request):

    if(request.method == "POST"):		
        request_body = json.loads(request.body)
        object_id = request_body.get("id")
        
        region_helper = RegionHelper()  
        response_status, response_data = region_helper.find_by_id(object_id)  
        
        return HttpResponse(
            json.dumps(response_data),
            content_type = "application/json",
            status = response_status
        )
    else:
        return redirect('/region/')

def region_delete(request):

    if(request.method == "DELETE"):		
        request_body = json.loads(request.body)
        object_id = request_body["id"]
        
        region_helper = RegionHelper()  
        response_status, response_data = region_helper.delete_by_id(object_id)

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json",
            status = response_status
        )
    else:
        return redirect('/region/')

def region_report(request):

    path= 'location/reports/region.html'
    region_helper = RegionHelper()  
    context = region_helper.make_report_context()
    html_template = render_to_string(path, context)
    pdf_file = HTML(string=html_template).write_pdf()
    response = HttpResponse(pdf_file, content_type='application/pdf')
    response['Content-Disposition'] = 'filename="reporte_regiones.pdf"'
    return response
