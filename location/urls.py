from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),    
    path('city', views.city, name='city'),    
    path('city/save', views.city_save, name='city_save'),    
    path('city/edit', views.city_edit, name='city_edit'),    
    path('city/delete', views.city_delete, name='city_delete'), 
    path('city/report', views.city_report, name='city_report'),    
    path('region', views.region, name='region'),    
    path('region/save', views.region_save, name='region_save'),    
    path('region/edit', views.region_edit, name='region_edit'),    
    path('region/delete', views.region_delete, name='region_delete'),    
    path('region/report', views.region_report, name='region_report'),    
    
]