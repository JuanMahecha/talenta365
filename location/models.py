from django.db import models

ACTIVE = 'A'
INACTIVE = 'I'
STATUS_CHOICES = [
    (ACTIVE, 'ACTIVO'),
    (INACTIVE, 'INACTIVO'),                
]

class City(models.Model):    
    
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    code = models.IntegerField(unique=True)
    text = models.CharField(max_length=255)	
    status = models.CharField(max_length=2,choices=STATUS_CHOICES, default=ACTIVE)	    
    
    def status_value(self):
        return dict(STATUS_CHOICES)[self.status]

    class Meta:
        db_table = 'city'

class Region(models.Model):

    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    code = models.IntegerField(unique=True)
    text = models.CharField(max_length=255)

    class Meta:
        db_table = 'region'

class RegionCity(models.Model): 
        
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    region = models.ForeignKey('Region', models.DO_NOTHING)
    city = models.ForeignKey('City', models.DO_NOTHING)
    status = models.CharField(max_length=2,choices=STATUS_CHOICES, default=ACTIVE)	    

    class Meta:
        db_table = 'region_city'