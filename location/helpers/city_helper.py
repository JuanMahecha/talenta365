from location.models import RegionCity, City, STATUS_CHOICES, ACTIVE

class CityHelper(object):

    def make_context(self):

        cities = City.objects.all().order_by('text')    
        context = {    
            "title": "MUNICIPIOS",
            "cities": cities, 
            "status_choices": STATUS_CHOICES,
        }
        return context
    def make_report_context(self):    

        cities = City.objects.all().order_by('text')            
        context = { 
            "cities": cities,         
        }
    
        return context

    def save(self, object_id, object_code, object_text, object_status):

        response_status = 200
        message = ""
        if object_code == "" or object_text == "" or object_id == "" or object_status == "":
            response_status = 400
            message = "Por favor complete los campos vacios, e intente de nuevo."
        else:
            objects_unique = City.objects.filter(code=object_code) 
            objects_db = City.objects.filter(id=object_id)            

            if len(objects_unique) > 0 and len(objects_db) <= 0:
                response_status = 400
                message = "El codigo ya existe, por favor digite otro."
            else:            
                if len(objects_db)>0:
                    message = "Registro editado con exito"
                    object_db = objects_db[0]
                else:  
                    message = "Registro creado con exito"
                    object_db = City()
                try:
                    object_db.code = object_code
                    object_db.text = object_text                  
                    object_db.status = object_status                 
                    object_db.save()     
                    RegionCity.objects.filter(city_id=object_db.id).update(status=object_status)                   
                except:
                    response_status = 400
                    message = "Registro no pudo ser creado o editado con exito" 
        
        response_data = {             
            "message": message,
        }
        return response_status, response_data
    
    def find_by_id(self, object_id):
        
        objects_db = City.objects.filter(id=object_id)        
        response_status = 200
        response_data = {}

        if len(objects_db) > 0:
            response_data = {                 
                "message": "",
                "city":  {
                    "id": objects_db[0].id,
                    "code": objects_db[0].code,
                    "text": objects_db[0].text,                    
                    "status": objects_db[0].status,                    
                }
            }
        else:
            response_status = 400
            response_data = {                 
                "message": "El registro no existe",                
            }
        
        return response_status, response_data
    
    def delete_by_id(self, object_id):
        
        objects_db = City.objects.filter(id=object_id)
        response_status = 200
        message = ""
        if len(objects_db)>0:
            try:
                object_db = objects_db[0]
                RegionCity.objects.filter(city_id=object_db.id).delete()                     
                object_db.delete()								
                message = "Registro eliminado con exito"
            except:
                response_status = 400
                message = "¡UPS! No fue posible eliminar el registro"
        else:
            response_status = 404
            message  = "El Registro no existe."

        response_data = {             
            "message": message,
        }
        
        return response_status, response_data
        