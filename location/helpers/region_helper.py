from location.models import RegionCity, Region, City, STATUS_CHOICES, ACTIVE

class RegionHelper(object):

    def make_context(self):
        cities_array = []
        cities_array_tmp = []
        cities = City.objects.filter(status=ACTIVE).order_by('text')    
        count = 1
        for city in cities:
            cities_array_tmp.append(city)
            if count >= 5 or count >= len(cities):                
                cities_array.append(cities_array_tmp)
                cities_array_tmp = []
                count = 0
            
            count += 1

        regions = Region.objects.all().order_by('text')            
        context = {   
            "title": "REGIONES", 
            "cities": cities_array,         
            "regions": regions,         
        }
    
        return context
        
    def make_report_context(self):    

        regions = Region.objects.all().order_by('text') 
        regions_resp = []
        for region in regions:
            cities = RegionCity.objects.filter(region_id=region.id, status=ACTIVE)
            cities_list = []
            for c in cities:                
                cities_list.append(str(c.city.code)+" - "+c.city.text)
            
            cities_str = ", ".join(cities_list)            
            region_resp = {
                "id": region.id,
                "created_at": region.created_at,
                "code": region.code,
                "text": region.text,  
                "cities": cities_str,                              
            }
            regions_resp.append(region_resp)       

        context = { 
            "regions": regions_resp,                     
        }
    
        return context

    def save(self, object_id, object_code, object_text, object_cities_id):
        
        response_status = 200
        message = ""
        if object_code == "" or object_text == "" or object_id == "":
            response_status = 400
            message = "Por favor complete los campos vacios, e intente de nuevo."
        else:
            objects_unique = Region.objects.filter(code=object_code) 
            objects_db = Region.objects.filter(id=object_id)            

            if len(objects_unique) > 0 and len(objects_db) <= 0:
                response_status = 400
                message = "El codigo ya existe, por favor digite otro."
            else:            
                if len(objects_db)>0:
                    message = "Registro editado con exito"
                    object_db = objects_db[0]
                else:  
                    message = "Registro creado con exito"
                    object_db = Region()
                try:
                    object_db.code = object_code
                    object_db.text = object_text                                      
                    object_db.save()  
                    
                    RegionCity.objects.filter(region_id=object_db.id).delete()                     
                    for city in object_cities_id:
                    
                        rc = RegionCity()
                        rc.region_id = object_db.id
                        rc.city_id = city
                        rc.status = ACTIVE
                        rc.save()
                except:
                    response_status = 400
                    message = "Registro no pudo ser creado o editado con exito" 
        
        response_data = {             
            "message": message,
        }
        return response_status, response_data
    
    def find_by_id(self, object_id):
        
        objects_db = Region.objects.filter(id=object_id)    
        cities = RegionCity.objects.filter(region_id=object_id, status=ACTIVE)
        cities_resp = []
        for c in cities:
            city = {
                "region_id": c.region_id,
                "city_id": c.city_id,                
            }
            cities_resp.append(city)

        response_status = 200
        response_data = {}

        if len(objects_db) > 0:
            response_data = {                 
                "message": "",
                "region":  {
                    "id": objects_db[0].id,
                    "code": objects_db[0].code,
                    "text": objects_db[0].text,  
                    "cities": cities_resp,                              
                }
            }
        else:
            response_status = 400
            response_data = {                 
                "message": "El registro no existe",                
            }
        
        return response_status, response_data
    
    def delete_by_id(self, object_id):
        
        objects_db = Region.objects.filter(id=object_id)
        response_status = 200
        message = ""
        if len(objects_db)>0:
            try:
                object_db = objects_db[0]
                RegionCity.objects.filter(region_id=object_db.id).delete()                     
                object_db.delete()								
                message = "Registro eliminado con exito"
            except:
                response_status = 400
                message = "¡UPS! No fue posible eliminar el registro"
        else:
            response_status = 404
            message  = "El Registro no existe."

        response_data = {             
            "message": message,
        }
        
        return response_status, response_data
        