#TALENTA365

##LOCATION
##Te permite crear Regiones y Municipios, asi como especificar que Municipios contiene las Regiones y descargar reportes en formato PDF.

###Base de datos: PostgreSQL
###Backend: Python/Django
###Frontend: HTML-JS/JQuery-CSS
###Bibliotecas: en archivo requirements.txt
###Puedes ejecutarlo de la siguiente forma:

1. Crea un virtualenv
`virtualenv -p python3 talenta365env`
2. Activar el entorno virtual
`cd talenta365env`
`source bin/activate`
3. Clona el proyecto
`git clone https://JuanMahecha@bitbucket.org/JuanMahecha/talenta365.git`
4. Instala los requerimientos
`cd talenta365`
`pip install -r requirements.txt`
5. Crea una base de datos y un usuario o actualiza los settings a tu preferencia
`CREATE USER user_talenta365 WITH PASSWORD 'RzPYN8K9!';`
`CREATE DATABASE db_talenta365;`
`GRANT ALL PRIVILEGES ON DATABASE "db_talenta365" to user_talenta365;`
6. Ejecuta migraciones
`python3 manage.py migrate`
7. Corre el proyecto
`python3 manage.py runserver`
8. Visualiza el proyecto desde el navegador ingresando a la siguiente direccion `http://127.0.0.1:8000`

##ABECEDARIO
### Es un aplicativo de consola, puedes ejecutarlo de la siguiente forma:
`python alphabet.py` -o- `python3 alphabet.py`


