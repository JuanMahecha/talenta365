
function openModal()
{
	$("#title-modal").html("Nuevo");		
	$('#modal').show();
	$("#id").val(0);
	$("#id").prop('disabled', false);
    $("#code").val("");
	$("#text").val("");		
	$("#message-modal").html("");	
	cleanCitiesChecks();
}
function cleanCitiesChecks(){
	var cities = $('input[name="city-check[]"]:checked');
	cities.each(function(){
		$(this).prop("checked", false);
	});
}
function saveObject(value){
	var cities = $('input[name="city-check[]"]:checked');
	
	var citiesId = [];
	cities.each(function(){
		citiesId.push($(this).val());
	});
	$.ajax({
		headers: {
			'X-CSRFToken': getCSRFToken(),
			'Content-Type': 'application/json; charset=utf-8',
		},
		data: JSON.stringify({
			"id": $("#id").val(),
			"code": $("#code").val(),
			"text": $("#text").val(),	
			"cities_id": citiesId,
		}),
		type: "POST",
		dataType: "json",
		url: "/region/save",
	})
	 .done(function( data, textStatus, jqXHR ) {		
		$('#modal').hide();					
		showAlert(data.message, "success", true);
	 })
	 .fail(function( jqXHR, textStatus, errorThrown ) {			
		$("#message-modal").html(jqXHR.responseJSON.message);	
	});
}
function editObject(value){
	$.ajax({
		headers: {
			'X-CSRFToken': getCSRFToken(),
			'Content-Type': 'application/json; charset=utf-8',
		},
		data: JSON.stringify({
			"id" : value
		}),
		type: "POST",
		dataType: "json",
		url: "/region/edit",
	})
	 .done(function( data, textStatus, jqXHR ) {		
		$("#title-modal").html("Editar");		
		$('#modal').show();
		$("#id").val(data.region.id);
		$("#id").prop('disabled', true);
		$("#code").val(data.region.code);
		$("#text").val(data.region.text);		
		$("#message-modal").html("");
		cleanCitiesChecks();
		$.each( data.region.cities, function( key, value ) {
			$("#cc"+value.city_id).prop("checked", true);
		});
	 })
	 .fail(function( jqXHR, textStatus, errorThrown ) {		
		showAlert(jqXHR.responseJSON.message, "error", true);
	});
}

function deleteObject(value){
	
	confirmation = confirm("Esta accion sera permanente. ¿Esta seguro que desea eliminar el registro?");
	if(confirmation != true)
	{			
		return;
	}

	$.ajax({
		headers: {
			'X-CSRFToken': getCSRFToken(),
			'Content-Type': 'application/json; charset=utf-8',
		},
		data: JSON.stringify({
			"id" : value
		}),
		type: "DELETE",
		dataType: "json",
		url: "/region/delete",
	})
	 .done(function( data, textStatus, jqXHR ) {	
		showAlert(jqXHR.responseJSON.message, "success", true);
	 })
	 .fail(function( jqXHR, textStatus, errorThrown ) {
		showAlert(jqXHR.responseJSON.message, "error", true);
	});
}
