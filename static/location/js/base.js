var timeReload = 800;
var makeReload = false;

function goURL(url)
{
    window.location.href = url
}

function goNewTab(url){
	window.open(url,'_blank');
	window.open(url);
}

function getCSRFToken() {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, 10) == ('csrftoken' + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(10));
                break;
            }
        }
    }
    return cookieValue;
}
function showAlert(message, type, reload){
	var color = "";
	makeReload = reload;
	switch (type) {
		case "success":			
			color = "#00d461";
			break;
		case "error":	  
			color = "#bf2c2c";
			break;
		case "warning":
			color = "#d4d400";
			break;
		case "info":
			color = "#001c38";
			break;
		default:
			color = "#13a5d1";
			break;
	}	
	$("#alert-box").html(message);	
	$("#alert-box").css("color", color);	
	$("#alert-box").css("box-shadow", $("#alert-box").css("box-shadow") + color);
	$("#alert-box").show();
	$("html, body").animate({ scrollTop: 0}, timeReload, function() { 
		setTimeout(function() {
			if(makeReload){
				location.reload();
			}else{
				$("#alert-box").html("");
				$("#alert-box").hide();
			}
		}, timeReload)
	});
}
