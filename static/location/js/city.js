
function openModal()
{
	$("#title-modal").html("Nuevo");		
	$('#modal').show();
	$("#id").val(0);
	$("#id").prop('disabled', false);
    $("#code").val("");
	$("#text").val("");	
	$("#status").val("");
	$("#message-modal").html("");
}

function saveObject(value){
	$.ajax({
		headers: {
			'X-CSRFToken': getCSRFToken(),
			'Content-Type': 'application/json; charset=utf-8',
		},
		data: JSON.stringify({
			"id": $("#id").val(),
			"code": $("#code").val(),
			"text": $("#text").val(),
			"status": $("#status").val(),
		}),
		type: "POST",
		dataType: "json",
		url: "/city/save",
	})
	 .done(function( data, textStatus, jqXHR ) {		
		$('#modal').hide();					
		showAlert(data.message, "success", true);
	 })
	 .fail(function( jqXHR, textStatus, errorThrown ) {			
		$("#message-modal").html(jqXHR.responseJSON.message);	
	});
}
function editObject(value){
	$.ajax({
		headers: {
			'X-CSRFToken': getCSRFToken(),
			'Content-Type': 'application/json; charset=utf-8',
		},
		data: JSON.stringify({
			"id" : value
		}),
		type: "POST",
		dataType: "json",
		url: "/city/edit",
	})
	 .done(function( data, textStatus, jqXHR ) {		
		$("#title-modal").html("Editar");		
		$('#modal').show();
		$("#id").val(data.city.id);
		$("#id").prop('disabled', true);
		$("#code").val(data.city.code);
		$("#text").val(data.city.text);
		$("#status").val(data.city.status);
		$("#message-modal").html("");
	 })
	 .fail(function( jqXHR, textStatus, errorThrown ) {		
		showAlert(jqXHR.responseJSON.message, "error", true);
	});
}

function deleteObject(value){
	
	confirmation = confirm("Esta accion sera permanente. ¿Esta seguro que desea eliminar el registro?");
	if(confirmation != true)
	{			
		return;
	}

	$.ajax({
		headers: {
			'X-CSRFToken': getCSRFToken(),
			'Content-Type': 'application/json; charset=utf-8',
		},
		data: JSON.stringify({
			"id" : value
		}),
		type: "DELETE",
		dataType: "json",
		url: "/city/delete",
	})
	 .done(function( data, textStatus, jqXHR ) {	
		showAlert(jqXHR.responseJSON.message, "success", true);
	 })
	 .fail(function( jqXHR, textStatus, errorThrown ) {
		showAlert(jqXHR.responseJSON.message, "error", true);
	});
}
